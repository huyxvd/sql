USE MASTER -- nhảy vô master database
GO -- thực thi lệnh
DROP DATABASE IF EXISTS TEST_KHOA_NGOAI -- xóa db nếu tồn tại
GO -- thực thi lệnh
CREATE DATABASE TEST_KHOA_NGOAI -- tạo ra db mới
GO -- thực thi lệnh
USE TEST_KHOA_NGOAI -- nhảy vô db
GO -- thực thi lệnh
CREATE TABLE Student (
  StudentID INT PRIMARY KEY,
  FullName NVARCHAR(100),
  DateOfBirth DATE
);
GO -- Tạo bảng student

CREATE TABLE StudentEmail (
  StudentID INT,
  Email NVARCHAR(100),
  -- FOREIGN KEY (StudentID) REFERENCES Student(StudentID) -- cách thường sau khi đã tạo bảng Student
);
GO -- Tạo bảng StudentEmail

-- thêm khóa ngoại sau khi 2 bảng đã được tạo rồi
ALTER TABLE StudentEmail
ADD FOREIGN KEY (StudentID)  REFERENCES Student(StudentID);
