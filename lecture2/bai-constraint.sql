

USE master -- nhảy vô master database
GO -- thực thi lệnh trước đó
DROP DATABASE IF EXISTS LECTURE2_CLASS_PRACTICE -- xóa database nếu đã tồn tại
GO
CREATE DATABASE LECTURE2_CLASS_PRACTICE -- tạo database
GO
USE LECTURE2_CLASS_PRACTICE -- sử dụng database vừa tạo
GO

/*
Tạo bảng "Student":
Cột StudentID được định nghĩa là khóa chính (PRIMARY KEY) và có kiểu dữ liệu INT. Nó sẽ tự động tăng lên mỗi khi thêm một hàng mới vào bảng (AUTO_INCREMENT).
Cột FullName có kiểu dữ liệu NVARCHAR(100) và không được phép NULL (NOT NULL).
Cột DateOfBirth có kiểu dữ liệu DATE và mặc định là ngày '2000-10-22' (DEFAULT '2000-10-22').
Cột Score có kiểu dữ liệu INT và có ràng buộc CHECK để đảm bảo rằng giá trị nằm trong khoảng từ 0 đến 100.
*/
CREATE TABLE Student(
	StudentID int PRIMARY KEY IDENTITY(1, 1), -- StudentID start from 1 and increase one by one.
	FullName nchar(50) NOT NULL, -- FullName is not nullable
	DateOfBirth date DEFAULT '10/22/2000', -- DateOfBirth default is '10/22/2000'
	Score int CHECK (Score > 0 AND Score < 100) -- Score between 0 and 100
)
GO
/*
Tạo bảng "StudentEmail":

Cột StudentID và Email cùng đóng vai trò là khóa chính (PRIMARY KEY) của bảng "StudentEmail".
Cột Email có ràng buộc UNIQUE để đảm bảo rằng giá trị của nó là duy nhất trong bảng.
Cột StudentID có khóa ngoại (FOREIGN KEY) liên kết với cột StudentID trong bảng "Student".
*/

CREATE TABLE StudentEmail(
	StudentID int FOREIGN KEY REFERENCES Student(StudentID),
	Email varchar(20) UNIQUE, -- Email is unique
	-- in 'StudentEmail' table Primary key is StudentID & Email
	CONSTRAINT PK_StudentEmail PRIMARY KEY (StudentID, Email),)
GO

