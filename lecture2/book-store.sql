USE master -- nhảy vô master database
GO -- thực thi lệnh trước đó
DROP DATABASE IF EXISTS BookStore -- Tạo cơ sở dữ liệu BookStore
GO
CREATE DATABASE BookStore
GO
USE BookStore -- sử dụng database vừa tạo
GO

-- Tạo bảng Books
CREATE TABLE Books (
  ID INT PRIMARY KEY IDENTITY(1, 1),
  Title VARCHAR(255),
  Author VARCHAR(255),
  Price DECIMAL(10,2),
  PublishedDate DATE
);

GO
-- Tạo bảng Customers
CREATE TABLE Customers (
  ID INT PRIMARY KEY IDENTITY(1, 1),
  Name VARCHAR(255),
  Email VARCHAR(255),
  Phone VARCHAR(20)
);
GO
-- Tạo bảng Orders
CREATE TABLE Orders (
  ID INT PRIMARY KEY IDENTITY(1, 1),
  CustomerID INT,
  OrderDate DATE,
  TotalAmount DECIMAL(10,2),
  FOREIGN KEY (CustomerID) REFERENCES Customers(ID)
);
GO
-- Tạo bảng OrderDetail
CREATE TABLE OrderDetail (
  ID INT PRIMARY KEY IDENTITY(1, 1),
  OrderID INT REFERENCES Orders(ID),
  BookID INT REFERENCES Books(ID),
  Count INT
);