USE master -- nhảy vô master database
GO -- thực thi lệnh trước đó
DROP DATABASE IF EXISTS LibraryManagement -- Tạo cơ sở dữ liệu LibraryManagement
GO
CREATE DATABASE LibraryManagement
GO
USE LibraryManagement -- sử dụng database vừa tạo
GO

-- Tạo bảng Books
CREATE TABLE Books (
  BookID INT PRIMARY KEY IDENTITY(1, 1),
  Title VARCHAR(255) NOT NULL,
  Author VARCHAR(255) NOT NULL,
  YearPublished INT,
  Quantity INT NOT NULL
);

GO

-- Tạo bảng BorrowerCards
CREATE TABLE BorrowerCards (
  CardID INT PRIMARY KEY  IDENTITY(1, 1),
  MemberName VARCHAR(100) NOT NULL,
  RegistrationDate DATE
);

GO

-- Tạo bảng Borrowings
CREATE TABLE Borrowings (
  BorrowID INT PRIMARY KEY  IDENTITY(1, 1),
  CardID INT REFERENCES BorrowerCards(CardID),
  BookID INT REFERENCES Books(BookID),
  BorrowDate DATE,
  DueDate DATE
);
