USE MASTER
GO
DROP DATABASE IF EXISTS BreakLimit
GO

CREATE DATABASE BreakLimit;
GO

USE BreakLimit;
GO

CREATE TABLE BreakTheLimit(
	Data char(8000) NOT NULL,
	MoreData char(54) NOT NULL
)
-- Lỗi này xuất hiện khi bạn cố gắng tạo một bảng với số lượng cột và dữ liệu đủ lớn để vượt quá kích thước tối đa của một dòng trong SQL Server
-- Trong SQL Server, kích thước tối đa của một dòng là 8060 byte + 7 byte overhead