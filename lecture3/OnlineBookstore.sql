USE MASTER
GO
DROP DATABASE IF EXISTS OnlineBookstore
GO
-- Tạo cơ sở dữ liệu
CREATE DATABASE OnlineBookstore
GO

-- Sử dụng cơ sở dữ liệu
USE OnlineBookstore;

-- Tạo bảng Books
CREATE TABLE Books (
	ID INT PRIMARY KEY IDENTITY(1, 1),
	Title VARCHAR(255) NOT NULL,
	Author VARCHAR(255) NOT NULL,
	Price DECIMAL(10,2) NOT NULL CHECK (Price > 0),
	PublishedDate DATE NOT NULL
);

-- Tạo bảng Customers
CREATE TABLE Customers (
	ID INT PRIMARY KEY IDENTITY(1, 1),
	FirstName VARCHAR(255) NOT NULL,
	LastName VARCHAR(255) NOT NULL,
	Email VARCHAR(255) UNIQUE NOT NULL,
	Phone VARCHAR(20) NOT NULL,
	DOB DATE NOT NULL
);

-- Tạo bảng Orders
CREATE TABLE Orders (
	ID INT PRIMARY KEY IDENTITY(1, 1),
	CustomerID INT FOREIGN KEY REFERENCES Customers(ID),
	OrderDate DATE NOT NULL,
	TotalAmount DECIMAL(10,2) NOT NULL CHECK (TotalAmount > 0),
	-- FOREIGN KEY (CustomerID) REFERENCES Customers(ID) các khác để thêm khóa ngoại
);

-- Tạo bảng OrderDetail
CREATE TABLE OrderDetail (
	OrderID INT FOREIGN KEY REFERENCES Orders(ID),
	BookID INT FOREIGN KEY REFERENCES Books(ID),
	NumberOfBook INT NOT NULL DEFAULT 1,
	CONSTRAINT PK_OrderID_BookID PRIMARY KEY(OrderID, BookID)
);

--1 Thêm vào từng bảng 10 bản ghi (sử dụng lệnh insert multiple row), chú ý đến IDENTITY có thể vào trang https://www.mockaroo.com/ để tạo ra dữ liệu giả
--SELECT
--a. Lấy tất cả sách từ bảng Books.
--b. Lấy tất cả khách hàng từ bảng Customers.
--c. Lấy tất cả đơn hàng từ bảng Orders.
--d. Lấy tất cả chi tiết đơn hàng từ bảng OrderDetail.
--e. Lấy sách có ID 10 từ bảng Books.
--f. Lấy khách hàng có ID 10 từ bảng Customers.
--g. Lấy đơn hàng có ID 10 từ bảng Orders.
--h. Lấy chi tiết đơn hàng có OrderID 1 từ bảng OrderDetail.

--3. UPDATE
--a. Cập nhật giá sách có ID 5 thành 14.99.
--b. Cập nhật email của khách hàng có ID 2 thành "newemail@gmail.com".
--c. Cập nhật tổng giá trị của đơn hàng có ID 4 thành 30.99.

--4. DELETE
--a. Xóa sách có ID 10 từ bảng Books.
--b. Xóa khách hàng có ID 10 từ bảng Customers.
--c. Xóa đơn hàng có ID 10 từ bảng Orders (bao gồm cả chi tiết đơn hàng).

--ADDITIONAL
--Viết câu lệnh SELECT để lấy 10 cuốn sách có giá cao nhất từ bảng Books.
--Viết câu lệnh SELECT để lấy "Title và Price" của tất cả sách, sử dụng tên định danh "BookName" cho cột Title.
--Viết câu lệnh SELECT để lấy tất cả tên tác giả không trùng lặp từ bảng Books.
--Viết VIEW hiển thị BookID, Book Title, Author và Published Date cho tất cả sách trong bảng Books.
--Viết câu lệnh SELECT INTO để tạo một bảng mới có tên "ExpensiveBooks" chứa các cột BookID, Title và Price của tất cả sách có giá lớn hơn $50.00

--Operators cơ bản:
--Viết truy vấn lấy tất cả khách hàng có tên chứa chuỗi "John".(gợi ý LIKE)
--Viết truy vấn lấy tất cả sách có giá lớn hơn hoặc bằng $20.
--Viết truy vấn lấy tất cả khách hàng có số điện thoại khác "555-1234".
--Viết truy vấn lấy tất cả đơn hàng có tổng giá trị nhỏ hơn $100.
--Operators logic:
--Viết truy vấn lấy tất cả khách hàng có tên chứa chuỗi "John" và địa chỉ email chứa chuỗi "gmail.com".