--- EX 2
USE MASTER
GO
DROP DATABASE IF EXISTS OnlineStore
GO
CREATE DATABASE OnlineStore
GO

USE OnlineStore
GO

CREATE TABLE Customers (
    CustomerID INT PRIMARY KEY IDENTITY(1, 1),
    FirstName VARCHAR(50) NOT NULL,
    LastName VARCHAR(50) NOT NULL,
    Email VARCHAR(100) UNIQUE NOT NULL,
    Address VARCHAR(200) NOT NULL
);

CREATE TABLE Orders (
    OrderID INT PRIMARY KEY IDENTITY(1, 1),
    OrderDate DATE NOT NULL,
    CustomerID INT FOREIGN KEY REFERENCES Customers(CustomerID),
	TotalAmount DECIMAL(10, 2) NOT NULL CHECK (TotalAmount > 100)
    
);

CREATE TABLE Product (
    ProductID INT PRIMARY KEY IDENTITY(1, 1),
    Name VARCHAR(100) NOT NULL,
    Description VARCHAR(200),
    Price DECIMAL(10, 2) NOT NULL CHECK (Price > 100)
);

CREATE TABLE Category (
    CategoryID INT PRIMARY KEY IDENTITY(1, 1),
    Name VARCHAR(50) NOT NULL,
    Description VARCHAR(200)
);

CREATE TABLE OrderItem (
    OrderID INT FOREIGN KEY REFERENCES Orders(OrderID),
    ProductID INT FOREIGN KEY REFERENCES Product(ProductID),
    Quantity INT NOT NULL DEFAULT 1,
    CONSTRAINT PK_OrderID_ProductID PRIMARY KEY (OrderID, ProductID),
);

CREATE TABLE ProductCategory (
    ProductID INT FOREIGN KEY REFERENCES Product(ProductID),
    CategoryID INT FOREIGN KEY REFERENCES Category(CategoryID),
    CONSTRAINT PK_ProductID_CategoryID PRIMARY KEY (ProductID, CategoryID)
);

--1 insert to each tables 10 records(use command insert multiple row) becareful with IDENTITY

--SELECT:

--Write a SQL query to select all the records from the "Customers" table.

SELECT *
FROM Customers;

--Write a SQL query to select the customer ID, first name, and last name of the customer whose email is 'example@example.com'.

SELECT CustomerID, FirstName, LastName
FROM Customers
WHERE Email = 'example@example.com';

--TOP:

--Write a SQL query to select the top 10 records from the "Product" table.

SELECT TOP 10 *
FROM Product;

--Write a SQL query to select the top 5 records from the "Customers" table where the address is '123 Main Street'.

SELECT TOP 5 *
FROM Customers
WHERE Address = '123 Main Street';

--PERCENT:

--Write a SQL query to select the top 25 percent of records from the "Product" table based on the price ascending.

SELECT TOP 25 PERCENT *
FROM Product
ORDER BY Price ASC;

--Write a SQL query to select the top 50 percent of records from the "Orders" table based on the order date descending.

SELECT TOP 50 PERCENT *
FROM Orders
ORDER BY OrderDate DESC;

--ORDER:

--Write a SQL query to select all records from the "Product" table, ordered by price in ascending order.

SELECT *
FROM Product
ORDER BY Price ASC;

--Write a SQL query to select all records from the "Orders" table, ordered by order date in descending order.

SELECT *
FROM Orders
ORDER BY OrderDate DESC;

--ALIAS:

--Write a SQL query to select the first name and last name of all customers, and display the results with column aliases "First Name" and "Last Name".

SELECT FirstName AS "First Name", LastName AS "Last Name"
FROM Customers;

--Write a SQL query to select the product ID and price of all products, and display the results with the column aliases "Product ID" and "Price (USD)".

SELECT ProductID AS "Product ID", Price AS "Price (USD)"
FROM Product;

--DISTINCT:

--Write a SQL query to select all unique values of the "Address" column from the "Customers" table.

SELECT DISTINCT Address
FROM Customers;

--Write a SQL query to select all unique values of the "Name" column from the "Category" table.

SELECT DISTINCT Name
FROM Category;

--WHERE:

--Write a SQL query to select the product name and price of all products that have a price greater than 200.

SELECT Name, Price
FROM Product
WHERE Price > 200;

--Write a SQL query to select the first name and last name of all customers who have an address containing the word "Avenue".

SELECT FirstName, LastName
FROM Customers
WHERE Address LIKE '%Avenue%';

--VIEW:

--Write a SQL query to create a view named "HighPricedProducts" that displays all products with a price greater than 1000.

CREATE VIEW HighPricedProducts AS
SELECT *
FROM Product
WHERE Price > 1000;

--Write a SQL query to create a view named "AllOrders" that displays orders.


CREATE VIEW AllOrders AS
SELECT *
FROM Orders;
--SELECT INTO:

--Write a SQL query to create a new table named "CopyProduct" that includes the product name, price, and description columns from the "Product" table.

SELECT ProductName, Price, Description
INTO CopyProduct
FROM Product;

--Write a SQL query to create a new table named "CustomersFromNY" that includes the first name, last name, and email columns from the "Customers" table, for customers with an address in New York.

SELECT FirstName, LastName, Email
INTO CustomersFromNY
FROM Customers
WHERE Address LIKE '%New York%';

--Authentic Operators:

--Write a SQL query to select all records from the "Product" table where the description is not null.

SELECT *
FROM Product
WHERE Description IS NOT NULL;

--Write a SQL query to select all records from the "Category" table where the description is null.

SELECT *
FROM Category
WHERE Description IS NULL;



--Compare Operators

--Write a SQL query to select all records from the "Product" table where the price is greater than or equal to 500.

SELECT *
FROM Product
WHERE Price >= 500;

--Write a SQL query to select all records from the "Orders" table where the TotalAmount greater than or equal to 500.

SELECT *
FROM Orders
WHERE TotalAmount >= 500;

--Logical Operators:

--Write a SQL query to select all records from the "Product" table where the price is greater than 1000 and the description is not null.

SELECT *
FROM Product
WHERE Price > 1000 AND Description IS NOT NULL;

--Write a SQL query to select all records from the "Orders" table where TotalAmount value is greater than 1000.

SELECT *
FROM Orders
WHERE TotalAmount > 1000;


--Note: Please make sure to use appropriate SQL syntax and follow proper data manipulation rules to perform these exercises.

