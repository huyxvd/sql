USE MASTER
GO
DROP DATABASE IF EXISTS OnlineStore
GO
CREATE DATABASE OnlineStore
GO

USE OnlineStore
GO

CREATE TABLE Customers (
  CustomerID INT PRIMARY KEY IDENTITY(1, 1),
  FirstName VARCHAR(50) NOT NULL,
  LastName VARCHAR(50) NOT NULL,
  Email VARCHAR(100) UNIQUE NOT NULL,
  Address VARCHAR(200) NOT NULL
);

CREATE TABLE Orders (
  OrderID INT PRIMARY KEY IDENTITY(1, 1),
  OrderDate DATE NOT NULL,
  CustomerID INT FOREIGN KEY REFERENCES Customers(CustomerID),
  TotalAmount DECIMAL(10, 2) NOT NULL CHECK (TotalAmount > 100)

);

CREATE TABLE Product (
  ProductID INT PRIMARY KEY IDENTITY(1, 1),
  Name VARCHAR(100) NOT NULL,
  Description VARCHAR(200),
  Price DECIMAL(10, 2) NOT NULL CHECK (Price > 100)
);

CREATE TABLE Category (
  CategoryID INT PRIMARY KEY IDENTITY(1, 1),
  Name VARCHAR(50) NOT NULL,
  Description VARCHAR(200)
);

CREATE TABLE OrderItem (
  OrderID INT FOREIGN KEY REFERENCES Orders(OrderID),
  ProductID INT FOREIGN KEY REFERENCES Product(ProductID),
  Quantity INT NOT NULL DEFAULT 1,
  CONSTRAINT PK_OrderID_ProductID PRIMARY KEY (OrderID, ProductID),
);

CREATE TABLE ProductCategory (
  ProductID INT FOREIGN KEY REFERENCES Product(ProductID),
  CategoryID INT FOREIGN KEY REFERENCES Category(CategoryID),
  CONSTRAINT PK_ProductID_CategoryID PRIMARY KEY (ProductID, CategoryID)
);

--1 Thêm vào từng bảng 10 bản ghi (sử dụng lệnh insert multiple row), chú ý đến IDENTITY có thể vào trang https://www.mockaroo.com/ để tạo ra dữ liệu giả

--SELECT:

--Viết truy vấn SQL để lấy tất cả bản ghi từ bảng "Customers".

--Viết truy vấn SQL để lấy Customer ID, first name và last name của khách hàng có email là 'example@example.com'.

--TOP:

--Viết truy vấn SQL để lấy 10 bản ghi đầu tiên từ bảng "Product".

--Viết truy vấn SQL để lấy 5 bản ghi đầu tiên từ bảng "Customers" với điều kiện địa chỉ là '123 Main Street'.

--PERCENT:

--Viết truy vấn SQL để lấy 25% bản ghi đầu tiên từ bảng "Product" dựa trên giá tăng dần.

--Viết truy vấn SQL để lấy 50% bản ghi đầu tiên từ bảng "Orders" dựa trên ngày đặt hàng giảm dần.

--ORDER:

--Viết truy vấn SQL để lấy tất cả bản ghi từ bảng "Product", sắp xếp theo giá tăng dần.

--Viết truy vấn SQL để lấy tất cả bản ghi từ bảng "Orders", sắp xếp theo ngày đặt hàng giảm dần.

--ALIAS:

--Viết truy vấn SQL để lấy first name và last name của tất cả khách hàng, và hiển thị kết quả với các định danh cột "First Name" và "Last Name".

--Viết truy vấn SQL để lấy Product ID và Price của tất cả sản phẩm, và hiển thị kết quả với các định danh cột "Product ID" và "Price (USD)".

--DISTINCT:

--Viết truy vấn SQL để lấy tất cả giá trị duy nhất của cột "Address" từ bảng "Customers".

--Viết truy vấn SQL để lấy tất cả giá trị duy nhất của cột "Name" từ bảng "Category".

--WHERE:

--Viết truy vấn SQL để lấy tên và giá của tất cả sản phẩm có giá lớn hơn 200.

--Viết truy vấn SQL để lấy first name và last name của tất cả khách hàng có địa chỉ chứ