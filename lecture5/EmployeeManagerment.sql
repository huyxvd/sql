USE MASTER
GO
DROP DATABASE IF EXISTS EmployeeManagerment
GO

CREATE DATABASE EmployeeManagerment;
GO

USE EmployeeManagerment;
GO

CREATE TABLE Departments (
    ID INT PRIMARY KEY,
    Name VARCHAR(50) NOT NULL,
    ManagerId INT 
);

-- Create tables
CREATE TABLE Employees (
    Id INT PRIMARY KEY,
    Name VARCHAR(50) NOT NULL,
    DepartmentId INT,
    Salary INT NOT NULL,
    ManagerId INT 
);


-- Insert data into Departments table
INSERT INTO Departments (ID, Name, ManagerId) VALUES
(1, 'Marketing', 1),
(2, 'Sales', 2),
(3, 'IT', 3),
(4, 'HR', 4),
(5, 'FUN', 0);

-- Insert data into Employees table
INSERT INTO Employees (Id, Name, DepartmentId, Salary, ManagerId) VALUES
(1, 'John', 1, 50000, 1),
(2, 'Jane', 2, 60000, 2),
(3, 'Bob', 3, 70000, 3),
(4, 'Sue', 4, 55000, 4),
(5, 'Dave', 1, 45000, 1),
(6, 'Alice', 2, 65000, 2),
(7, 'Fred', 3, 80000, 3),
(8, 'Mary', 4, 50000, 4),
(9, 'Tom', 1, 55000, 8),
(10, 'Kate', 2, 70000, 7),
(11, 'Haha', 1000, 70000, 5);

SELECT *
FROM Departments

SELECT *
FROM Employees

--INNER JOIN

--Lấy tên và phòng ban của mỗi nhân viên, cùng với tên của người quản lý phòng ban.
--Lấy tên và lương của mỗi nhân viên, cùng với tên của phòng ban mà họ làm việc.
--Lấy tên và lương của mỗi nhân viên làm việc trong phòng ban IT.
--Lấy tên của mỗi phòng ban và tổng lương của tất cả nhân viên làm việc trong phòng ban đó.
--OUTER JOIN

--Lấy tên và phòng ban của mỗi nhân viên, cùng với tên của người quản lý phòng ban (bao gồm cả nhân viên không có người quản lý).
--Lấy tên và lương của mỗi nhân viên, cùng với tên của phòng ban mà họ làm việc (bao gồm cả nhân viên không có phòng ban).
--Lấy tên và lương của mỗi nhân viên làm việc trong phòng ban IT (bao gồm cả nhân viên không có phòng ban).
--Lấy tên của mỗi phòng ban và tổng lương của tất cả nhân viên làm việc trong phòng ban đó (bao gồm cả phòng ban không có nhân viên).
--EXCLUDING JOIN:

--Viết một câu truy vấn để lấy tất cả nhân viên không có phòng ban.
--Viết một câu truy vấn để lấy tất cả phòng ban không có nhân viên.
--Viết một câu truy vấn để lấy tất cả nhân viên không có người quản lý.
--Viết một câu truy vấn để lấy tất cả người quản lý không có nhân viên trong phòng ban của họ.
--SELF JOIN:

--Viết một câu truy vấn để lấy tất cả nhân viên và tên của người quản lý của họ.
--Viết một câu truy vấn để lấy tất cả nhân viên và đồng nghiệp cùng phòng ban.
--Viết một câu truy vấn để lấy tất cả phòng ban và tên của người quản lý.
--Viết một câu truy vấn để lấy tất cả phòng ban và tên của nhân viên.
--UNION và UNION ALL:

--Viết một câu truy vấn để lấy tất cả nhân viên trong phòng ban Marketing và tất cả nhân viên trong phòng ban Sales sử dụng toán tử UNION.
--Viết một câu truy vấn để lấy tất cả nhân viên trong phòng ban Marketing và tất cả nhân viên trong phòng ban Sales sử dụng toán tử UNION ALL.
--Viết một câu truy vấn để lấy tất cả nhân viên trong phòng ban Marketing và tất cả nhân viên trong phòng ban Sales và sắp xếp theo lương giảm dần.
--Viết một câu truy vấn để lấy tất cả nhân viên trong phòng ban Marketing và tất cả nhân viên trong phòng ban Sales và loại bỏ bất kỳ bản sao nào sử dụng toán tử UNION.