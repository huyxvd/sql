USE MASTER
GO
DROP DATABASE IF EXISTS LECTURE5_JOIN_DEMO
GO
CREATE DATABASE LECTURE5_JOIN_DEMO
GO
USE LECTURE5_JOIN_DEMO
GO
CREATE TABLE KhachHang(
	ID int PRIMARY KEY,
	HoTen nvarchar(20) NOT NULL
);
CREATE TABLE DiaChiGiaoHang(
	ID int PRIMARY KEY,
	DiaChiDayDu nvarchar(20) NOT NULL
);
 
CREATE TABLE DonHang(
	ID int PRIMARY KEY,
	KhachHangID int,
	TenMonAn nvarchar(20) NOT NULL,
	DiaChiGiaoHangID int
);

INSERT INTO KhachHang(ID, HoTen)
VALUES(1, N'Châu Tinh Trì'),
		(2, N'Châu Nhuận Phát'),
		(3, N'Lý Tiểu Long'),
		(4, N'Thành Long'),
		(5, N'Trương Học Hữu');

INSERT INTO DiaChiGiaoHang(ID, DiaChiDayDu)
VALUES(1, N'Hồ Chí Minh'),
		(2, N'Đà Nẵng'),
		(3, N'Cần Thơ');
 
INSERT INTO DonHang(ID, KhachHangID, TenMonAn, DiaChiGiaoHangID)
VALUES(1, 1, N'Heo Quay', 1),
		(2, 1, N'Gà Luộc', 1),
		(3, 3, N'Bò Lá Lốt', 2),
		(4, 8, N'Phở', 2),
		(5, 10, N'Gà Nướng', 5)

SELECT *
FROM KhachHang


SELECT *
FROM DiaChiGiaoHang

SELECT *
FROM DonHang
