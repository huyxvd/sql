-- Khởi tạo một ID ngẫu nhiên và in ra giá trị của nó
DECLARE @ID UNIQUEIDENTIFIER
SET @ID = NEWID()
SELECT @ID AS RANDOM_ID
PRINT(@ID)

-- Chuẩn bị dữ liệu để demo
-- Sử dụng cơ sở dữ liệu master
USE MASTER
GO

-- Xóa cơ sở dữ liệu nếu đã tồn tại
DROP DATABASE IF EXISTS LECTURE7_INDEX_DEMO
GO

-- Tạo cơ sở dữ liệu mới
CREATE DATABASE LECTURE7_INDEX_DEMO
GO

-- Sử dụng cơ sở dữ liệu vừa tạo
USE LECTURE7_INDEX_DEMO
GO

-- Tạo bảng TableA với cột SearchColumn cột này sẽ chứa giá trị random ngẫu nhiên
CREATE TABLE TableA(
    SearchColumn NVARCHAR(36) NOT NULL
)

-- Chèn 10.000.000 bản ghi ngẫu nhiên vào bảng TableA
DECLARE @i INT = 500000 -- 500.000 vòng lặp * 20 bản ghi = 10.000.000 bản ghi
WHILE @i > 0
BEGIN
    INSERT INTO TableA(SearchColumn)
    VALUES
    (NEWID()), -- 1
    (NEWID()), -- 2
    (NEWID()), -- 3
    (NEWID()), -- 4
    (NEWID()), -- 5
    (NEWID()), -- 6
    (NEWID()), -- 7
    (NEWID()), -- 8
    (NEWID()), -- 9
    (NEWID()), -- 10
    (NEWID()), -- 11
    (NEWID()), -- 12
    (NEWID()), -- 13
    (NEWID()), -- 14
    (NEWID()), -- 15
    (NEWID()), -- 16
    (NEWID()), -- 17
    (NEWID()), -- 18
    (NEWID()), -- 19
    (NEWID()) -- 20
    SET @i = @i - 1
END
PRINT('DONE insert 10.000.000 record')

-- Đếm số lượng bản ghi trong bảng TableA
SELECT COUNT(*) FROM TableA

-- Chèn một bản ghi đặc biệt vào bảng TableA để tiến hành so sánh performance
INSERT INTO TableA(SearchColumn) VALUES (N'Hello INDEX')

-- Sao chép dữ liệu từ TableA sang TableB
SELECT *
INTO TableB
FROM TableA

-- Kiểm tra số lượng bản ghi trong cả hai bảng
SELECT COUNT(*) FROM TableA
SELECT COUNT(*) FROM TableB

-- Thực hiện truy vấn tìm kiếm bản ghi đặc biệt trong cả hai bảng, trước khi tạo index
SELECT SearchColumn FROM TableA WHERE SearchColumn = N'Hello INDEX'
SELECT SearchColumn FROM TableB WHERE SearchColumn = N'Hello INDEX'

-- Tạo index trên bảng TableB
CREATE INDEX IDX_HELLO_INDEX ON TableB (SearchColumn)

-- Thực hiện lại truy vấn tìm kiếm bản ghi đặc biệt sau khi tạo index
SELECT SearchColumn FROM TableA WHERE SearchColumn = N'Hello INDEX'
SELECT SearchColumn FROM TableB WHERE SearchColumn = N'Hello INDEX'
