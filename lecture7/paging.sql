USE MASTER
GO
DROP DATABASE IF EXISTS LECTURE7_PAGING
GO
CREATE DATABASE LECTURE7_PAGING
GO
USE LECTURE7_PAGING
GO

CREATE TABLE Customers (
    Id INT PRIMARY KEY,
    FullName NVARCHAR(50) NOT NULL
);

GO
-- Thêm 10 bản ghi vào bảng Customers
INSERT INTO Customers (Id, FullName)
VALUES                (1, N'Nguyễn Văn A'),
                      (2, N'Trần Thị B'),
                      (3, N'Lê Văn C'),
                      (4, N'Phạm Thị D'),
                      (5, N'Hoàng Văn E'),
                      (6, N'Vũ Thị F'),
                      (7, N'Trương Văn G'),
                      (8, N'Ngô Thị H'),
                      (9, N'Mai Văn I'),
                      (10, N'Lý Thị K'),
                      (11, N'Huỳnh Văn L'),
                      (12, N'Đặng Thị M'),
                      (13, N'Phan Văn N'),
                      (14, N'Nguyễn Thị P'),
                      (15, N'Lê Văn Q');

SELECT *
FROM Customers
