USE MASTER
GO

DROP DATABASE IF EXISTS SQL_injection -- Xóa cơ sở dữ liệu nếu đã tồn tại
GO

CREATE DATABASE SQL_injection -- Tạo cơ sở dữ liệu và chuyển vào cơ sở dữ liệu đó
GO

USE SQL_injection
GO

-- Tạo bảng Users và chèn dữ liệu mẫu
CREATE TABLE Users (
  ID INT PRIMARY KEY IDENTITY(1, 1),
  Username VARCHAR(100) NOT NULL,
  Password VARCHAR(100) NOT NULL
)

INSERT INTO Users (Username, Password) 
VALUES ('user1', 'pass1'),
       ('user2', 'pass2')

-- Tạo bảng SensitiveData và chèn dữ liệu mẫu
CREATE TABLE SensitiveData (
    Name NVARCHAR(50),
    CreditCardNumber NVARCHAR(50)
)

INSERT INTO SensitiveData (Name, CreditCardNumber) 
VALUES ('John Doe', '1234 5678 9012 3456'), 
       ('Jane Smith', '9876 5432 1098 7654')
