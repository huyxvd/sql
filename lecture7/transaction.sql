USE MASTER
GO

-- Xóa cơ sở dữ liệu nếu đã tồn tại để bắt đầu từ đầu
DROP DATABASE IF EXISTS SQL_TRANSACTION
GO

-- Tạo một cơ sở dữ liệu mới có tên SQL_TRANSACTION
CREATE DATABASE SQL_TRANSACTION
GO

-- Chuyển sang cơ sở dữ liệu vừa tạo
USE SQL_TRANSACTION
GO

-- Tạo bảng Users để lưu thông tin người dùng bao gồm số dư trong ví
CREATE TABLE Users (
    Id INT PRIMARY KEY,
    FullName NVARCHAR(50),
    Wallet MONEY CHECK (Wallet >= 0)  -- Đảm bảo cột Wallet không âm
)

-- Chèn dữ liệu ban đầu vào bảng Users
INSERT INTO Users (Id, FullName, Wallet)
VALUES (1, 'A', 10),
       (2, 'B', 0)

-- Giả lập việc chuyển tiền giữa hai người dùng A và B
DECLARE @SoTien MONEY
SET @SoTien = 3  -- Đặt số tiền cần chuyển

-- Trừ tiền từ tài khoản A (Id = 1)
UPDATE Users
SET Wallet = Wallet - @SoTien
WHERE Id = 1

-- Cộng tiền vào tài khoản B (Id = 2)
UPDATE Users
SET Wallet = Wallet + @SoTien
WHERE Id = 2

-- Hiển thị thông tin người dùng sau khi chuyển tiền
SELECT *
FROM Users

-- Sử dụng giao dịch để đảm bảo tính nhất quán dữ liệu trong quá trình chuyển tiền
BEGIN TRANSACTION

-- Khai báo số tiền cần chuyển trong phạm vi giao dịch
DECLARE @SoTienchuyen MONEY
SET @SoTienchuyen = 3

BEGIN TRY
    -- Trừ tiền từ tài khoản A trong giao dịch
    UPDATE Users
    SET Wallet = Wallet - @SoTienchuyen
    WHERE Id = 1
    
    -- Cộng tiền vào tài khoản B trong giao dịch
    UPDATE Users
    SET Wallet = Wallet + @SoTienchuyen
    WHERE Id = 2

    -- Commit giao dịch nếu tất cả các cập nhật thành công
    COMMIT TRANSACTION

    PRINT N'Giao dịch đã hoàn tất!'  -- In thông báo nếu giao dịch thành công
END TRY
BEGIN CATCH
    -- Rollback giao dịch nếu có lỗi xảy ra
    ROLLBACK TRANSACTION

    PRINT N'Giao dịch bị hủy bỏ do lỗi!'  -- In thông báo nếu giao dịch bị hủy bỏ do lỗi
END CATCH

-- Hiển thị thông tin người dùng sau khi giao dịch
SELECT *
FROM Users
